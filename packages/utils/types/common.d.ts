export interface IAnyObject {
  [key: string]: any;
}

export interface IFilterConfig {
  invalidKeys: string[];
  childKey: string;
}

export interface IHeader {
  "content-disposition"?: string;
  "Content-Disposition"?: string;
  ContentDisposition?: string;
}

/**
 * 📢📢📢：上下文介绍：https://km.sankuai.com/collabpage/2125672007
 * 
 * 参数说明：
    row：表格中每一行的数据信息，统称为 row。通常在进行该行的操作时，会携带该参数。
    key：对应wiki图中“参数名”一列的概念。通常在同一层级下是唯一的，但不能保证跨层级时的唯一性，比如上图中的 userId，就出现了 2 处，但均合理。
    id：row信息中的唯一标识，通常并不具备业务含义。注意和 key 的区别。
    pid：当前row对应的父类row信息id。
    treelist：树状列表数据。特殊情况下也可能只有一级。
 */
import { nanoid } from 'nanoid'

export const ID_ALIAS = {
  id: '_tl_id_',
  pid: '_tl_pid_',
}

export function makeUUID() {
  return nanoid(10)
}

/**
 * 给业务过来的 treelist 中的每一行 row 添加 id（包括pid）
 * @param treelist
 * @param options
 */
function wrapTreelistById(treelist, options) {
  treelist = treelist || []
  const { pid, idKey = ID_ALIAS.id, pidKey = ID_ALIAS.pid } = options || {}
  if (treelist.length === 0) {
    return []
  }

  const destList = treelist.map((item) => {
    const { children, ...rest } = item
    // 查出对应的同一级别下的原始数据
    const destId = makeUUID()
    return {
      ...rest,
      [idKey]: destId,
      [pidKey]: pid,
      children: wrapTreelistById(children, {
        ...options,
        pid: destId,
      }),
    }
  })
  // console.log('>>> wrapListByIdKey: ', list, destList)
  return destList
}

/**
 * 拼接全局唯一的key值，作为unionkey返回。
 * 举例：item = { name: 'userKey', value: '13' }, treeList = [
 *  { name: 'wrapDi', value: '1' , children: [
 *      { name: 'userId', value: '12' },
 *      { name: 'userKey', value: '13' },
 *    ]
 *  },
 * ]
 * getUnionkey(item, treeList, { key: 'name' }) → 'wrapDi.userKey'
 * @param {*} row {String | Object} 当前row元素/row对应的id
 * @param {*} treeList 树状列表
 * @param {String} options.key treelist列表中对应的key名称，常见的有key/name/code/…
 * @param {String} options.gap 父子层级key的衔接符，默认“.”
 * @returns
 */
function getUnionkey(row, treelist, options) {
  treelist = treelist || []
  const { key = 'key', idKey = ID_ALIAS.id, pidKey = ID_ALIAS.pid, gap = '.' } = options || {}

  const list2Map = (list = [], map = {}) =>
    list.reduce((pre, cur) => {
      pre[cur[idKey]] = cur
      if (cur.children) {
        list2Map(cur.children, pre)
      }
      return pre
    }, map)

  // 先依据rowId把List转为Map，方便定位
  const listMap = list2Map(treelist)
  row = typeof row === 'string' ? listMap[row] : row
  if (row == null) {
    throw Error('[getUnionkey]: treelist中没定位到对应的row')
  }

  // 存储所有的父类key(包括自身)
  const keys = [row[key]]
  let parentId = row[pidKey]
  while (parentId) {
    const parentRow = listMap[parentId]
    if (parentRow) {
      keys.unshift(parentRow.name)
      parentId = parentRow[pidKey]
    } else {
      parentId = null
    }
  }
  return keys.join(gap)
}

/**
 * 把原始打平的Array数据转换为带有Children的Tree状Array数据
 * @param {Array} list
 * @param {String} options.idKey id对应的键名称 通常就是 id/uuid/_id_..
 * @param {String} options.pidKey pid对应的键名称 通常就是 pid/parentId..
 * @param {String} options.childrenKey children对应的键名称，通常就是 children
 * @returns
 */
function list2Treelist(list, options) {
  const obj = {} // 重新存储数据
  const res = [] // 存储最后结果
  const len = list.length
  const { idKey = 'id', pidKey = 'pid', childrenKey = 'children' } = options || {}

  // 遍历原始数据data，构造obj数据，键名为id，值为数据
  for (let i = 0; i < len; i++) {
    obj[list[i][idKey]] = list[i]
  }

  // 遍历原始数据
  for (let j = 0; j < len; j++) {
    const listItem = list[j]
    // 通过每条数据的 pid 去obj中查询
    const parentList = obj[listItem[pidKey]]
    if (parentList) {
      // 根据 pid 找到的是父节点，list是子节点，
      if (!parentList[childrenKey]) {
        parentList[childrenKey] = []
      }
      // 将子节点插入 父节点的 children 字段中
      parentList[childrenKey].push(listItem)
    } else {
      // pid 找不到对应值，说明是根结点，直接插到根数组中
      res.push(listItem)
    }
  }
  return res
}

/**
 * 从树状列表中萃取出全局唯一名称对应的目标元素.
 * unionkey = 'wrapDi.userId', list = [
 *  { name: 'wrapDi', value: '1' , children: [
 *      { name: 'userId', value: '12' },
 *      { name: 'userKey', value: '13' },
 *    ]
 *  },
 * ]
 * getRowByUnionkey(unionkey, list) → { name: 'userId', value: '12' }
 * @param {String} unionkey 全局唯一名称
 * @param {Array} treelist 树状列表
 * @param {String} options.key row中的key键名称
 * @param {String} options.gap 父子层级key的衔接符，默认“.”
 */
function getRowByUnionkey(unionkey = '', treelist, options) {
  treelist = treelist || []
  const { key = 'key', gap = '.' } = options || {}

  const keys = unionkey.split(gap)
  let destItem = treelist.find((param) => param[key] === keys[0])
  for (let i = 1; i < keys.length; i++) {
    destItem = destItem?.children?.find((param) => param[key] === keys[i])
  }
  return destItem || {}
}

export { getRowByUnionkey, getUnionkey, list2Treelist, wrapTreelistById }

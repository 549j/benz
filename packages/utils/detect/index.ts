import { workerFunction, createWorker } from "./helper";

export interface versionPollingOptions {
  $cat?: any;
  appETagKey?: string;
  pollingInterval?: number;
  htmlFileUrl?: string; // 待检测更新的HTML文件
  closeDetection?: boolean; // hidden时是否需要关闭轮询
  immediate?: boolean;
  onUpdate?: Function;
}

export interface settingOptions {
  time?: number;
  closeDetection?: boolean;
  onUpdate?: Function;
}

class VersionPolling {
  options: versionPollingOptions = {};
  defaultOptions = {
    $cat: null,
    appETagKey: "__APP_ETAG__",
    htmlFileUrl: `${location.origin}${location.pathname}`,
    closeDetection: false,
  };
  appEtag = "";
  myWorker: null | Worker = null;
  currentVisibilityChangeHandle: any = null;

  constructor(options: versionPollingOptions) {
    this.init(options);
  }

  async init(options: versionPollingOptions) {
    this.currentVisibilityChangeHandle = this.handleVisibilityChange.bind(this);
    this.options = Object.assign({}, this.defaultOptions, options);
    if (!this.options.pollingInterval) {
      this.options.pollingInterval = 1 * 60; // 默认1分钟检测一次
    }
    this.options.pollingInterval *= 1000;

    await this.updateEtag();
    this.start();
  }

  start() {
    this.myWorker = createWorker(workerFunction);
    this.sendWorkerMsg();
    this.myWorker.onerror = (e) => {
      console.log("createVersionPolling worker error:", e);
    };
    this.myWorker.onmessage = (event) => {
      const { appETagKey, lastEtag, etag } = event.data;
      this.appEtag = etag;
      if (this.options.appETagKey === appETagKey && lastEtag !== etag) {
        this.stop();
        this.noticeHandle();
      }
    };
    document.addEventListener("visibilitychange", this.currentVisibilityChangeHandle);
  }

  stop() {
    if (this.myWorker) {
      document.removeEventListener("visibilitychange", this.currentVisibilityChangeHandle);
      this.myWorker.terminate();
      this.myWorker = null;
    }
  }
  onRefresh() {
    sessionStorage.setItem(`${this.options.appETagKey}`, this.appEtag);
    window.location.reload();
  }
  /**
   * 页面隐藏时停止轮询任务，页面再度可见时继续
   */
  handleVisibilityChange() {
    const { closeDetection } = this.options;
    if (!this.myWorker || closeDetection) return;

    const isHidden = document.visibilityState === "hidden";
    const message = {
      code: isHidden ? "pause" : "resume",
    } as any;
    if (!isHidden) {
      const { appETagKey, pollingInterval, htmlFileUrl } = this.options;
      message.data = {
        appETagKey,
        htmlFileUrl,
        pollingInterval,
        lastEtag: sessionStorage.getItem(`${this.options.appETagKey}`),
      };
    }
    this.myWorker.postMessage(message);
  }

  continuePolling(type = false, time = 60) {
    if (type && typeof time === "number" && !isNaN(time)) {
      setTimeout(() => {
        // 再次开始检测
        this.start();
      }, time * 1000);
      this.options?.$cat?.logInfo("[ops 子应用再次检测，时间：", time * 1000);
    } else {
      sessionStorage.removeItem(`${this.options.appETagKey}`);
      this.options?.$cat?.logInfo("[ops 子应用取消再次检测");
    }
  }

  defaultNotice() {
    const result = confirm("页面有更新，点击确定刷新页面！");
    // 这里可以做埋点
    if (result) {
      this.options?.$cat?.logInfo("[ops 更新提示 默认确认]");
      this.onRefresh();
    } else {
      this.options?.$cat?.logInfo("[ops 更新提示 默认取消");
      this.continuePolling(false);
    }
  }

  noticeHandle() {
    this.options?.$cat?.logInfo("[ops 检测到版本更新");
    if (this.options.onUpdate) {
      this.options?.$cat?.logInfo("[ops 更新提示 子应用自定义更新执行", this.options.onUpdate.toString());
      this.options.onUpdate(this);
    } else {
      this.defaultNotice();
    }
  }

  async updateOptions(options: versionPollingOptions) {
    this.myWorker?.postMessage({
      code: "pause",
    });
    this.options = Object.assign({}, this.defaultOptions, options);
    if (!this.options.pollingInterval) {
      this.options.pollingInterval = 5 * 60;
    }
    this.options.pollingInterval *= 1000;
    await this.updateEtag();
    this.sendWorkerMsg();
  }

  sendWorkerMsg() {
    const { appETagKey, pollingInterval, htmlFileUrl, closeDetection } = this.options;
    const lastEtag = sessionStorage.getItem(`${appETagKey}`);
    if (!lastEtag) return;

    const msgData = {
      code: closeDetection ? "pause" : "start",
      data: {
        appETagKey,
        htmlFileUrl,
        pollingInterval,
        lastEtag,
        random: Math.random(),
      },
    };
    this.myWorker?.postMessage(msgData);
  }

  async updateEtag() {
    const { htmlFileUrl } = this.options;
    if (!htmlFileUrl) return;

    fetch(htmlFileUrl, { method: "HEAD", cache: "no-cache" })
      .then((res) => {
        const etag = res.headers.get("etag");
        if (etag) {
          this.appEtag = etag;
          sessionStorage.setItem(`${this.options.appETagKey}`, etag);
        }
      })
      .catch(() => {});
  }

  async changeSetting(options: settingOptions) {
    this.myWorker?.postMessage({
      code: "pause",
    });
    let { time } = options;
    if (time) {
      time *= 1000;
    }
    this.options.pollingInterval = time;
    if (options.hasOwnProperty("closeDetection")) {
      this.options.closeDetection = Boolean(options.closeDetection);
    }
    if (options.hasOwnProperty("onUpdate")) {
      this.options.onUpdate = options.onUpdate;
    }
    await this.updateEtag();
    this.sendWorkerMsg();
  }
}

let versionInstance: VersionPolling | null = null;

export function createVersionPolling(options: versionPollingOptions) {
  if (!versionInstance) {
    versionInstance = new VersionPolling(options);
  } else {
    versionInstance.updateOptions(options);
  }
  return versionInstance;
}

export function workerFunction() {
  let timer: ReturnType<typeof setTimeout> | null = null;
  self.onmessage = (event) => {
    const code = event.data.code;
    // 离开页面停止检测
    if (code === "pause") {
      timer && clearInterval(timer);
      timer = null;
      return;
    }

    let { htmlFileUrl, lastEtag, appETagKey, pollingInterval, random = "" } = event.data.data;
    const runReq = () => {
      fetch(`${htmlFileUrl}`, {
        method: "HEAD",
        cache: "no-cache",
      }).then((response) => {
        const etag = response.headers.get("etag");
        if (lastEtag && etag && lastEtag !== etag) {
          timer && clearInterval(timer);
          timer = null;
          self.postMessage({
            appETagKey,
            lastEtag,
            etag,
            random,
          });
        }
      });
    };
    runReq();
    if (!timer) {
      timer = setInterval(runReq, pollingInterval);
    }
  };
  return self;
}

/**
 * 创建一个Web Worker，它允许在后台线程中运行一个函数，而不会干扰主线程的执行。
 * @param func
 * @returns
 */
export function createWorker(func: Function) {
  const blob = new Blob([`(${func.toString()})()`], { type: "text/javascript" });
  const url = window.URL.createObjectURL(blob);
  const worker = new Worker(url);
  window.URL.revokeObjectURL(url);
  return worker;
}

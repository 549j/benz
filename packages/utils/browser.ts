/**
 * 浏览器宿主环境下的相关方法
 */

import { IHeader } from "./types/common";

/**
 * 是否为浏览器宿主环境
 * @returns {boolean}
 */
function isBrowser() {
  return typeof window !== "undefined" && !!window.document;
}

/**
 * 从headers.disposition中解析出下载文件名
 * @param {*} headers
 * @param {*} defaultFilename
 * @returns
 */
function parseFilename(headers: IHeader = {}, defaultFilename: string) {
  let filename = "";
  // https://stackoverflow.com/questions/40939380/how-to-get-file-name-from-content-disposition
  const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
  const disposition =
    headers["content-disposition"] || headers["Content-Disposition"] || headers["ContentDisposition"] || "";
  const matches = filenameRegex.exec(disposition);
  if (matches != null && matches[1]) {
    filename = matches[1].replace(/['"]/g, "");
  }
  return filename || defaultFilename || "默认文件名.zip";
}

interface IdownloadResult {
  success: boolean; // 下载成功 or 失败
  message?: string; // 失败原因
}

/**
 * 针对后端返回Blob类型的数据下载实现
 * @param {Blob} blob 后端返回的Blob类型数据
 * @param {Object} options.resHeaders 后端返回响应体中的headers，主要用来获取后端返回的filename。
 * @param {String} options.filename 默认下载文件名
 * @returns {IdownloadResult}
 */
function downLoadBlobFile(blob: Blob, options: { resHeaders: IHeader; filename: string }) {
  if (!isBrowser())
    return {
      success: false,
      message: "当前环境非浏览器",
    } as IdownloadResult;

  const {
    resHeaders, // 响应头信息: response.headers
    filename, // 默认下载文件名
  } = options || {};

  // 响应数据不是Blob类型
  if (Object.prototype.toString.call(blob) !== "[object Blob]") {
    return {
      success: false,
      message: "非Blob类型数据",
    } as IdownloadResult;
  }

  try {
    // 用a标签触发下载机制
    const a = document.createElement("a");
    a.style.display = "none";
    a.href = window.URL.createObjectURL(new Blob([blob]));
    a.download = parseFilename(resHeaders, filename);
    document.body.appendChild(a);
    a.click(); // 执行下载
    window.URL.revokeObjectURL(a.href); // 释放url
    document.body.removeChild(a); // 释放标签

    return { success: true } as IdownloadResult;
  } catch (e: any) {
    return {
      success: false,
      message: e.message || "下载异常",
    } as IdownloadResult;
  }
}

export { isBrowser, downLoadBlobFile };
